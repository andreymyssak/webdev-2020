# About me

```sh
ФИ: Мысак Андрей
Группа: ВТиП-402
Роль: архитектор 🤓, кодер 🔥

Языки программирования: JavaScript, TypeScript
Технологии: React, React Native, Node.js 🤠
```

**Intuit**
1. "Web-технологии" [certificate](https://www.intuit.ru/verifydiplomas/101304598)
2. "Операционная система Linux" [certificate](https://intuit.ru/verifydiplomas/101378110)
3. "Введение в Django" [certificate](https://intuit.ru/verifydiplomas/101378145)
4. "Web-программирование на PHP 5.2" [certificate](https://intuit.ru/verifydiplomas/101378139)

**Coursera**
1. "Front-End Web UI Frameworks and Tools: Bootstrap 4" [certificate](https://coursera.org/share/16f2164f7567c90a874ae56a7e14d311)
2. "Front-End Web Development with React" [certificate](https://coursera.org/share/09912fcad36a96b9f6226deb1bb18b37)
3. "Multiplatform Mobile App Development with React Native" [certificate](https://coursera.org/share/7114347c7aded0d7d0e60b85914006ab)
4. "Server-side Development with NodeJS, Express and MongoDB" [certificate](https://coursera.org/share/579379b74bd05404e2adae9ec3075102)
