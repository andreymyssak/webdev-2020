import React, { FC, memo, useEffect, useState } from 'react';
import { Alert, Linking } from 'react-native';
import RNIap, {
  finishTransaction,
  InAppPurchase,
  PurchaseError,
  purchaseErrorListener,
  purchaseUpdatedListener,
  SubscriptionPurchase,
} from 'react-native-iap';
import BecomeGuideScreenView from './BecomeGuideScreenView';

const BecomeGuideScreenContainer: FC = (): JSX.Element => {
  const [isIapLoading, handleIapLoading] = useState(true);

  useEffect(() => {
    (async (): Promise<void> => {
      try {
        await RNIap.initConnection();
        console.log('sdf');
      } catch (err) {
        console.log('INIT_CONNECTION_ERROR', err.message);
      } finally {
        handleIapLoading(false);
      }
    })();
  }, []);

  useEffect(() => {
    const purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase: InAppPurchase | SubscriptionPurchase) => {
        const { transactionReceipt } = purchase;
        if (transactionReceipt) {
          try {
            await finishTransaction(purchase);
          } catch (err) {
            console.log('FINISH_TRANSACTION_ERROR', err);
          }
        }
      },
    );
    return (): void => {
      purchaseUpdateSubscription && purchaseUpdateSubscription.remove();
    };
  }, []);

  useEffect(() => {
    const purchaseErrorSubscription = purchaseErrorListener(
      (error: PurchaseError) => {
        Alert.alert('SUBSCRIPTION_ERROR', JSON.stringify(error));
      },
    );

    return (): void => {
      purchaseErrorSubscription && purchaseErrorSubscription.remove();
    };
  }, []);

  const onPressCancel = (): any =>
    Linking.openURL(
      'https://play.google.com/store/account/subscriptions?package=com.testappkz&sku=the_first_product_id',
    );

  return (
    <BecomeGuideScreenView
      isIapLoading={isIapLoading}
      onPressCancel={onPressCancel}
    />
  );
};

export default memo(BecomeGuideScreenContainer);
