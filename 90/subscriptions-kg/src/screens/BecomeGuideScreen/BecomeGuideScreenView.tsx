import React, { FC, memo } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { ActivityIndicatorSplash, Purchases } from './components';
import { Btn } from './components';

interface IProps {
  isIapLoading: boolean;
  onPressCancel: () => void;
}

const BecomeGuideScreenView: FC<IProps> = ({
  isIapLoading,
  onPressCancel,
}): JSX.Element => {
  if (isIapLoading) {
    return <ActivityIndicatorSplash />;
  }

  const renderHeader = (): JSX.Element => (
    <View style={styles.container}>
      <Btn text='Cancel subscription (android)' onPress={onPressCancel} />
    </View>
  );

  return (
    <ScrollView style={styles.scrollView}>
      {renderHeader()}
      <Purchases />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
  },
  container: {
    flex: 1,
    padding: 24,
  },
});

export default memo(BecomeGuideScreenView);
