import React, { FC, memo, useCallback, useEffect, useState } from 'react';
import { ActivityIndicator, Alert, Platform } from 'react-native';
import RNIap, { SubscriptionPurchase } from 'react-native-iap';
import { ISubscription } from '@types';
import PurchasesView from './PurchasesView';

const itemSubs: any = Platform.select({
  ios: [
    'become_a_guide_one_month',
    'become_a_guide_three_month',
    'become_a_guide_six_month',
    'become_a_guide_one_year',
  ],
  android: ['become_a_guide_one_month', 'the_first_product_id'],
});

const PurchasesContainer: FC = (): JSX.Element | null => {
  const [subscriptions, handleSubscriptions] = useState();

  useEffect(() => {
    (async (): Promise<void> => {
      try {
        const data = await RNIap.getSubscriptions(itemSubs);
        console.log('data', data);

        const formattedData: ISubscription[] = data
          .sort((a, b) => parseInt(a.price, 10) - parseInt(b.price, 10))
          .map((item, index) => ({
            data: item,
            isSelected: !index,
          }));

        handleSubscriptions(formattedData);
      } catch ({ code, message }) {
        console.log(code, message);
      }
    })();
  }, []);

  const onPressSubscription = useCallback(
    (id: string): void => {
      const newData = subscriptions.map(item => ({
        ...item,
        isSelected: item.data.productId === id,
      }));

      handleSubscriptions(newData);
    },
    [subscriptions],
  );

  const checkReceipt = (receipt: SubscriptionPurchase): Promise<Response> =>
    fetch('http://192.168.100.3:3000', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        appType: Platform.OS === 'android' ? 'android' : 'ios',
        purchase: receipt,
      }),
    });

  const onPressPurchase = useCallback(async (): Promise<void> => {
    try {
      const selectedSub = subscriptions.filter(item => item.isSelected)[0];
      const { productId } = selectedSub && selectedSub.data;

      const receipt = await RNIap.requestSubscription(productId, false);
      await checkReceipt(receipt);
    } catch (err) {
      Alert.alert('REQUEST_SUBSCRIPTION_ERROR', err.message);
    }
  }, [subscriptions]);

  const onPressRestore = useCallback(async (): Promise<void> => {
    try {
      const purchases = await RNIap.getAvailablePurchases();

      if (purchases && purchases.length > 0) {
        await checkReceipt(purchases[purchases.length - 1]);
      } else {
        Alert.alert('Oops', 'NO SUBSCRIPTIONS');
      }
    } catch (err) {
      Alert.alert('GET_AVAILABLE_PURCHASES_ERROR', err.message);
    }
  }, []);

  if (!subscriptions) {
    return <ActivityIndicator />;
  }

  return (
    <PurchasesView
      subscriptions={subscriptions}
      onPressSubscription={onPressSubscription}
      onPressPurchase={onPressPurchase}
      onPressRestore={onPressRestore}
    />
  );
};

export default memo(PurchasesContainer);
