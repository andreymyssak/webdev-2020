import React, { FC, memo } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { ISubscription } from '@types';
import { SubscriptionCheckbox, SubscriptionContent } from './components';

interface IProps {
  subscription: ISubscription;
  isLast: boolean;

  onPressSubscription: (id: string) => void;
}

const Subscription: FC<IProps> = ({
  subscription,
  isLast,

  onPressSubscription,
}): JSX.Element => {
  const { data, isSelected } = subscription;
  const {
    productId,
    currency,
    price,
    subscriptionPeriodAndroid,
    subscriptionPeriodNumberIOS,
    subscriptionPeriodUnitIOS,
  } = data;
  const periodType = subscriptionPeriodAndroid || subscriptionPeriodUnitIOS;

  const onPress = (): void => onPressSubscription(productId);

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.container, isLast && styles.isLast]}
    >
      <SubscriptionCheckbox isSelected={isSelected} />
      <SubscriptionContent
        currency={currency}
        price={price}
        periodCount={subscriptionPeriodNumberIOS}
        periodType={periodType}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    maxWidth: 300,
    backgroundColor: 'rgba(13, 95, 147, 0.1)',
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 12,
    marginBottom: 20,
  },
  isLast: {
    marginBottom: 0,
  },
});

export default memo(Subscription);
