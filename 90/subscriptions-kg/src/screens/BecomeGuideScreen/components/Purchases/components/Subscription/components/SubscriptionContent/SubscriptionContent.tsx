import React, { FC, memo, useMemo } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
  ANDROID_PERIOD_TYPE,
  IFormattedPeriod,
  IOS_PERIOD_TYPE,
  IPriceForPeriod,
} from '@types';
import { formatPeriod, formatPriceForPeriod, serializePeriod } from '@utils';
import SubscriptionBadge from './SubscriptionBadge';

interface IProps {
  currency: string;
  price: string;
  periodCount?: string;
  periodType?: ANDROID_PERIOD_TYPE | IOS_PERIOD_TYPE;
}

const SubscriptionContent: FC<IProps> = (data): JSX.Element => {
  const { currency, price, periodType, periodCount } = data;
  const localizedPrice = `${currency} ${price}`;

  const formattedPeriod: string = useMemo(
    () =>
      formatPeriod({
        countIOS: periodCount,
        type: periodType,
      }),
    [periodCount, periodType],
  );

  const serializedPeriod: IFormattedPeriod = useMemo(
    () =>
      serializePeriod({
        countIOS: periodCount,
        type: periodType,
      }),
    [periodCount, periodType],
  );

  const formattedPriceForPeriod: IPriceForPeriod = useMemo(
    () => formatPriceForPeriod(currency, price, serializedPeriod),
    [currency, price, serializedPeriod],
  );

  return (
    <View style={styles.content}>
      <View>
        <Text style={styles.price}>{localizedPrice}</Text>
        <Text style={styles.period}>{formattedPeriod}</Text>
      </View>
      <SubscriptionBadge data={formattedPriceForPeriod} />
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  price: {
    color: 'rgba(0,0,0,0.8)',
    fontSize: 14,
    fontWeight: 'bold',
  },
  period: {
    color: 'rgba(0,0,0,0.8)',
    fontSize: 14,
    marginTop: -2,
  },
});

export default memo(SubscriptionContent);
