import React, { FC, memo } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { ICONS } from '@static';

interface IProps {
  isSelected: boolean;
}

const SubscriptionCheckbox: FC<IProps> = ({ isSelected }): JSX.Element => {
  if (isSelected) {
    return <Image source={ICONS.filledRoundCheckbox} style={styles.icon} />;
  }

  return <View style={[styles.icon, styles.emptyIcon]} />;
};

const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
    marginRight: 16,
  },
  emptyIcon: {
    borderRadius: 12,
    backgroundColor: 'white',
  },
});

export default memo(SubscriptionCheckbox);
