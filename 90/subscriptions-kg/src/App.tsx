import * as React from 'react';
import { BecomeGuideScreen } from './screens';

const App = (): JSX.Element => <BecomeGuideScreen />;

export default App;
