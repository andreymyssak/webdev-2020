import { Subscription } from 'react-native-iap';

export type PERIOD_TYPE = 'day' | 'week' | 'month' | 'year' | 'invalid date';
export type IOS_PERIOD_TYPE = string | 'DAY' | 'WEEK' | 'MONTH' | 'YEAR';
export type ANDROID_PERIOD_TYPE =
  | string
  | 'P3D'
  | 'P7D'
  | 'P1W'
  | 'P1M'
  | 'P3M'
  | 'P6M'
  | 'P1Y';

export interface IPeriod {
  countIOS?: string;
  type?: ANDROID_PERIOD_TYPE | IOS_PERIOD_TYPE;
}

export interface IFormattedPeriod {
  count: number;
  type: PERIOD_TYPE;
}

export interface ISubscription {
  data: Subscription;
  isSelected: boolean;
}

export interface IPriceForPeriod {
  currency: string;
  price: number;
  type: PERIOD_TYPE;
}
