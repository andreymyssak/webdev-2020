const THEME = {
  colors: {
    primary: '#0D5F93',
    accent: '#FFF',
    error: '#CC3333',
  },
};

const COLORS = THEME.colors;

export { THEME, COLORS };
